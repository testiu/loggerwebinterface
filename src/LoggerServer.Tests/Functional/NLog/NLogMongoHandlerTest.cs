﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading;
using LoggerServer.Main.NLog;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using NLog.Config;
using NLog.LogReceiverService;
using NLog.Targets;

namespace LoggerServer.Tests.Functional.NLog
{
   // [TestClass]
    public class NLogMongoHandlerTest
    {
        private static global::NLog.Logger logger;
        string _uri = @"http://127.0.0.1:7777/NlogReceiver";

        [TestInitialize]
        public void SetUp()
        {
           
        }

        private void CreateService(ILogReceiverServer handler,string uri)
        {
            // настройка получателя
           // NLogMongoHandler nlog = new NLogMongoHandler();
            ServiceHost host = new ServiceHost(handler);
            host.AddServiceEndpoint(typeof(ILogReceiverServer), new BasicHttpBinding(), new Uri(uri));
            host.Open();
        }

        private LogReceiverWebServiceTarget CreateNetTarget(string uri)
        {
// настройка отправителя
            var logTarget = new LogReceiverWebServiceTarget();
            logTarget.EndpointConfigurationName = "nlog";
           // var logTarget = new MemoryTarget();
            
            logTarget.Name = "net";
            logTarget.EndpointAddress = uri;

            //logTarget.Layout = "Your layout format here";
            // e.g. "${logger}: ${message} ${exception:format=tostring}";

            // specify what gets logged to the above target
            var loggingRule = new LoggingRule("*", LogLevel.Debug, logTarget);

            // add target and rule to configuration
            LogManager.Configuration.AddTarget(logTarget.Name, logTarget);
            LogManager.Configuration.LoggingRules.Add(loggingRule);
            LogManager.Configuration.Reload();

            return logTarget;
        }

        [TestMethod]
        public void TestServerReceivesLogs()
        {
            var handler = new TestNlogSHendler();
            CreateService(handler,_uri);
            var t = CreateNetTarget(_uri);

            logger = LogManager.GetCurrentClassLogger();
            logger.Error("Sample error message");

            Thread.Sleep(2000);
          // Assert.IsTrue(t.Logs.Count > 0);
             Assert.IsTrue(handler.eventsHistory.Count == 1);
        }

        [TestMethod]
        public void TestMongoReceivesAndWritesLogs()
        {
            var handler = new NLogMongoHandler();
            CreateService(handler, _uri);
            CreateNetTarget(_uri);

            logger = LogManager.GetCurrentClassLogger();

            logger.Error("Sample error message");
            //TODO: add assert
           // Assert.IsTrue(handler.eventsHistory.Count == 1);
        }

    }
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
        ConcurrencyMode = ConcurrencyMode.Multiple)]
    class TestNlogSHendler:ILogReceiverServer
    {
        public List<NLogEvents> eventsHistory = new List<NLogEvents>(2); 
        public void ProcessLogMessages(NLogEvents events)
        {
            Debug.WriteLine("Inserting log");
            eventsHistory.Add(events);
        }
    }
}
