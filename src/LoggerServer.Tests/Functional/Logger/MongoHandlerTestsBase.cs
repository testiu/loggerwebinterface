﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Libs.Logger;
using Libs.Logger.Configuration;
using LoggerServer.Main.Logger.Handlers.Mongo;
using LogRepository.Entities;
using LogRepository.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoggerServer.Tests.Functional.Logger
{
    [TestClass]
    public class MongoHandlerTestsBase : IDisposable
    {
        protected LoggerMongoHandler _handler;
        private bool _initPassed;
        protected List<Tuple<LogLevel, string, string>> _logInfo;
        protected Libs.Logger.Logger _logger;
        protected IRepository<Log> _repo;
        protected Libs.Logger.Net.LoggerServer _tcpServer;

        protected string address = @"127.0.0.1";
        protected int port = 7777;

        protected virtual void CreateLogger()
        {
            LogWriterWrapperConfiguration writer = new NetWriterConfiguration(LogLevel.FullLog, address, port);
            var configuration = new LoggerConfiguration(LogLevel.FullLog, true, false, writer);
            _logger = LoggerFactory.CreateLogger("moduleTest", configuration);

            //List<Tuple<LogLevel, string, string>> logInfo;
        }

        protected virtual void CreateReceiver()
        {
            _repo = MongoTestHelpers.GetRepository();
            _handler = new LoggerMongoHandler(_repo);
            _tcpServer = Libs.Logger.Net.LoggerServer.CreateOnTcp(_handler, port);
        }

        protected virtual void StartReceiver()
        {
            _tcpServer.Open();
            _logger.Trace("initialize message");
            Thread.Sleep(500);
        }

        [TestInitialize]
        public virtual void SetUp()
        {
            if (_initPassed)
                return;

            _initPassed = true;
            // настройка отправителя
            CreateLogger();

            // настройка получателя
            CreateReceiver();
            StartReceiver();
        }


        [TestMethod]
        public void ReceiveLogsAndWritesToMongo()
        {
            int nlogs = 2000;
            var logGenerator = new GenerateLogsHelper();
            List<Log> logList = logGenerator.GenerateRandomLogs(nlogs);
            logList.ForEach(WriteRandomLog);

            WaitForEndOfWrite();
            Assert.IsTrue(MongoTestHelpers.IsLogsExist(logList, _repo));
        }

        //[TestMethod]
        public void WriteLogsWithIntervalsToMongo()
        {
            int nlogs = 1000;
            int sleepTime = 2000;
            var logGenerator = new GenerateLogsHelper();
            List<Log> logList = logGenerator.GenerateRandomLogs(nlogs);

            foreach (Log log in logList)
            {
                Thread.Sleep(sleepTime);
                WriteRandomLog(log);
            }

            WaitForEndOfWrite();
        }

        [TestMethod]
        public void ReceiveLogsAndWritesToMongo_MultiThread_FullCheck()
        {
            // Возможна ситуация, когда логи не успевают записаться в монгу до конца теста. Чтобы починить - использвать sleep(N)
            int numberThreads = 4;
            int nlogs = 50;
            var logList = new List<Log>();

            List<Thread> threads = new List<Thread>();
            for (var i = 0; i < numberThreads; i++)
            {
                var logGenerator = new GenerateLogsHelper();
                var list = logGenerator.GenerateRandomLogs(nlogs);
                logList.Union(list);
                threads.Add(new Thread(() => list.ForEach(WriteRandomLog)));
            }

            threads.ForEach(t => { t.Start(); });

            foreach (var thread in threads)
            {
                thread.Join();
            }

            WaitForEndOfWrite();
            Assert.IsTrue(MongoTestHelpers.IsLogsExist(logList, _repo));
        }

        protected void WaitForEndOfWrite()
        {
            int collectionSize = MongoTestHelpers.CollectionSize(_repo);
            int lastCollectionSize = -1;
            while (collectionSize > lastCollectionSize)
            {
                Thread.Sleep(100);
                lastCollectionSize = collectionSize;
                collectionSize = MongoTestHelpers.CollectionSize(_repo);
            }
        }

        public void WriteRandomLog(Log log)
        {
            System.Exception exc = new Exception("aaa");
            _logger.Log(MongoTestHelpers.GetLogLevel(log.LogLevel), exc, log.Message);
        }

        public void Dispose()
        {
            _repo = MongoTestHelpers.GetRepository();
            _repo = null;

            if (_tcpServer != null)
                _tcpServer.Dispose();
        }
    }
}