﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Libs.Logger;
using Libs.Logger.Configuration;
using LoggerServer.Main;
using LoggerServer.Main.Logger.Handlers.Mongo;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoggerServer.Tests.Functional.Logger
{
    //[TestClass]
    public class MongoHandlerTestReceiver : MongoHandlerTestsBase
    {
        protected string fileWriterPath = "fileWriter.txt";
        protected override void CreateLogger()
        {
            var queue = new AsyncQueueWrapperConfiguration(1000, true);
            var group = new GroupWrapperConfiguration();
            queue.InnerWriter = group;
            var netWriter = new NetWriterConfiguration(LogLevel.FullLog, address, port);

            System.IO.File.WriteAllText(fileWriterPath,string.Empty);
            var fileWriter = new FileWriterConfiguration(LogLevel.FullLog, FileWriterConfiguration.DefaultTemplateFormat,
                fileWriterPath, Encoding.UTF8);
            
            group.InnerWriters.Add(netWriter);
            group.InnerWriters.Add(fileWriter);


            var configuration = new LoggerConfiguration(LogLevel.FullLog, true, false, queue);
            _logger = LoggerFactory.CreateLogger("moduleTest", configuration);
        }
        protected override void CreateReceiver()
        {
            _repo = MongoTestHelpers.GetRepository();
            _handler = new LoggerMongoHandler(_repo);
            _tcpServer = Libs.Logger.Net.LoggerServer.CreateOnTcp(_handler, port);
        }

        virtual public void SetUp()
        {
            
        }

        [TestMethod]
        public void WhenNetReceiverStops_ContinueWritingToFile()
        {
            CreateLogger();

            CreateReceiver();
            StartReceiver();

            var nlogsBefore = 20;
            var nlogsAfter = 20;
            var logGenerator = new GenerateLogsHelper();
            var logListBefore = logGenerator.GenerateRandomLogs(nlogsBefore);
            logGenerator = new GenerateLogsHelper();
            var logListAfter = logGenerator.GenerateRandomLogs(nlogsAfter);

            logListBefore.ForEach(WriteRandomLog);

            Thread.Sleep(4000);
            _tcpServer.Dispose();
            logListAfter.ForEach(WriteRandomLog);
            Thread.Sleep(4000);
            _logger.Dispose();

            var count = 0;
            //var substr = DateTime.Now.Date.ToString("dd.mm.yyyy");
            var substr = "[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9][0-9]";

            Regex regexp = new Regex(substr);

            using (StreamReader reader = new StreamReader(fileWriterPath))
            {
                var fileContent = reader.ReadToEnd();
                //count = fileContent.Length;
                count = regexp.Matches(fileContent).Count;

            }
            Assert.AreEqual(nlogsBefore + nlogsAfter + 1, count);
        }

        [TestMethod]
        public void WhenNetReceiverNeverStarts_StillWriteToFile()
        {
        }
    }
}
