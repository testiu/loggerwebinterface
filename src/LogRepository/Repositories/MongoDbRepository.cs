﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using LogRepository.Entities;
using MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace LogRepository.Repositories
{
    /// <summary>
    /// A MongoDB repository. Maps to a collection with the same name
    /// as type TEntity.
    /// </summary>
    /// <typeparam name="T">Entity type for this repository</typeparam>
    public class MongoDbRepository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        private MongoDatabase _database;
        public MongoCollection<TEntity> Collection { get; set; }

        public MongoDbRepository()
        {
            GetDatabase();
            GetCollection();
        }

        public MongoDbRepository(string dbName, string collectionName)
        {
            var client = new MongoClient(ConfigurationManager
                .AppSettings
                .Get("MongoDbConnectionString")
                .Replace("{DB_NAME}", dbName));
            var server = client.GetServer();

            Collection = _database.GetCollection<TEntity>(typeof(TEntity).Name);
        }

        public MongoDbRepository(string connectionString, string dbName, string collectionName)
        {
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            _database = server.GetDatabase(dbName);
            Collection = _database.GetCollection<TEntity>(collectionName);
        }

        public bool Insert(TEntity entity)
        {
            return Collection.Insert(entity).Ok;
        }

        public bool Update(TEntity entity)
        {
            if (entity.Id == null)
                return Insert(entity);
        
            return Collection
                .Save(entity)
                .DocumentsAffected > 0;
        }

        public bool Delete(TEntity entity)
        {
            return Collection
                .Remove(Query.EQ("Id", entity.Id))
                .DocumentsAffected > 0;
        }

        public IList<TEntity> SearchFor(Expression<Func<TEntity, bool>> predicate)
        {
            return Collection
                .AsQueryable()
                .Where(predicate)
                .ToList();
            /*var query = Collection
                .AsQueryable<Log>()
                .Where(p => p.Date<DateTime.Now).Where(p => p.Date> new DateTime(2014,6,10, 18,14,21)).Where(p => p.LogLevel == Log.LoggerLevel.Debug);

            Libs.Logger.Logger.Default.Info(  ((MongoQueryable<TEntity>) query).GetMongoQuery().ToString());
            return (IList<TEntity>) query.ToList();*/
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return Collection.AsQueryable<TEntity>();
        }

        public IList<TEntity> GetAll()
        {
            return Collection.FindAllAs<TEntity>().ToList();
        }

        public void CreateIndex()
        {
            
        }

        public TEntity GetById(ObjectId id)
        {
            return Collection.FindOneByIdAs<TEntity>(id);
        }
         
        #region Private Helper Methods
        private void GetDatabase()
        {
            var client = new MongoClient(GetConnectionString());
            var server = client.GetServer();

            _database = server.GetDatabase(GetDatabaseName());
        }

        private string GetConnectionString()
        {
            return ConfigurationManager
                .AppSettings
                .Get("MongoDbConnectionString")
                .Replace("{DB_NAME}", GetDatabaseName());
        }

        private string GetDatabaseName()
        {
            return ConfigurationManager
                .AppSettings
                .Get("MongoDbDatabaseName");
        }

        private void GetCollection()
        {
            Collection = _database
                .GetCollection<TEntity>(typeof(TEntity).Name);
        }
        #endregion
    }
}