﻿using System.Diagnostics;
using System.IO;
using Libs.Logger;
using LoggerServer.Main.Logger;

namespace LogRepository.Repositories.Mongo
{
    public static class MongoStartHelper
    {
        private static Logger _logger = RepoLogger.Instance.GetThisClassLogger();
        public static Process StartMongoDb(string pathMongod, string pathData)
        {
            var startInfo = new ProcessStartInfo();
            startInfo.FileName = Path.Combine(Directory.GetCurrentDirectory(), pathMongod, "mongod.exe");
            string pathDataFull = Path.Combine(Directory.GetCurrentDirectory(), pathData);
            bool isExists = Directory.Exists(pathData);
            if (!isExists)
                Directory.CreateDirectory(pathData);

            startInfo.Arguments = "--dbpath " + pathDataFull;

            _logger.Info("Starting process: "+startInfo.Arguments);
            Process p = new Process();
            p.StartInfo = startInfo;
            
            p.Start();
            //p.WaitForExit();
            //_logger.Info(p.StandardOutput.ReadToEnd());

            return p;
        }

        private static void StartViaCmd(string pathMongod)
        {
            var process = new Process();
            var startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/c " + pathMongod + "mongod.exe";
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();
        }
    }
}