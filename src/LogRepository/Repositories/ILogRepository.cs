﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogRepository.Entities;
using MongoDB.Bson;

namespace LogRepository.Repositories
{
    public interface ILogRepository: IRepository<Log>
    {
        List<string> GetAllHosts();
        List<string> GetAllProcessNames();
        Log GetById(ObjectId id);
    }
}
