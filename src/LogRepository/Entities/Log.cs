﻿using System;
using System.Collections.Generic;
using Libs.Logger.Common;

namespace LogRepository.Entities
{
    public class Log : EntityBase
    {
        public enum LoggerLevel
        {
            Trace = 1,
            Debug = 2,
            Info = 3,
            Warn = 4,
            Error = 5,
            Fatal = 6
        };
        
        public string Assembly { get; set; }
        public string Class { get; set; }
        public string Context { get; set; }
        public DateTime Date { get; set; }
        public LogException Exception { get; set; }
        public string FilePath { get; set; }
        public string Host { get; set; }
        public LoggerLevel LogLevel { get; set; }
        public int LineNumber { get; set; }
        //public string MachineName { get; set; } теперь это Host
        public string Method { get; set; }
        public string Message { get; set; }
        public string ProcessName { get; set; }
        public List<string> Stacksource { get; set; }
        
    }

    public class LogException
    {
        public LogException(){}
        public LogException(Libs.Logger.Common.Error exception )
        {
            Message = exception.Message;
            Source = exception.Source;
            StackTrace = exception.StackTrace;
            Type = exception.Type;
            InnerException = exception.InnerError != null ? new LogException(exception.InnerError) : null;
        }

        public string Message { get; set; }
        public string Source { get; set; }
        public string StackTrace { get; set; }
        public string Type { get; set; }
        public LogException InnerException { get; set; }
    }
}