﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogRepository.Entities
{
    public class Process : EntityBase
    {
        public Process(string processName)
        {
            ProcessName = processName;
        }
        public string ProcessName { get; set; }
    }
}
