﻿using Libs.Logger;

namespace LoggerServer.Main.Logger
{
    public class RepoLogger : Libs.Logger.Logger
    {
        private static RepoLogger _instance = new RepoLogger(LogLevel.FullLog, ConsoleLogger);

        public static RepoLogger Instance
        {
            get { return _instance; }
        }

        private RepoLogger(LogLevel logLevel, ILogger innerLoggerWrapper)
            : base(logLevel, "Main", innerLoggerWrapper)
        {
        }

        [LoggerWrapperInitializationMethod]
        public static void Init(ILogger innerLogger)
        {
            _instance = new RepoLogger(innerLogger.Level, innerLogger);
        }
    }
}
