﻿using System;
using System.Collections.Generic;
using Libs.Logger.Common;
using LogRepository.Entities;

namespace LoggerServer.Main.Logger.Handlers.Mongo
{
    class LoggerConverter
    {

        private Random random = new Random();
        private DateTime GenerateDateTime()
        {
            var year = 2000 + random.Next(15);
            var month = 1 + random.Next(12);
            var day = 1 + random.Next(27);
            return new DateTime(year, month, day);
        }

        private string GenerateHost()
        {
            return "host " + random.Next(10);
        }

        public Log ConvertToMongoDb(LoggingEvent data)
        {
            var levels = new List<Log.LoggerLevel>()
            {
                Log.LoggerLevel.Trace,
                Log.LoggerLevel.Debug,
                Log.LoggerLevel.Info,
                Log.LoggerLevel.Warn,
                Log.LoggerLevel.Error,
                Log.LoggerLevel.Fatal
            };
            LogException exception = null;
            if (data.Exception!=null)
            {
                exception = new LogException(data.Exception);
            }
            var log = new Log()
            {
                Assembly = data.Assembly,
                Class = data.Clazz,
                Context = data.Context,
                Date = data.Date,
                //Date = GenerateDateTime(),
                Exception = exception,
                FilePath = data.FilePath,
                Host = data.MachineName,
                //Host = GenerateHost(),
                LogLevel = levels[data.Level.Level],
                LineNumber = data.LineNumber,
                Method = data.Method,
                Message = data.Message,
                ProcessName = data.ProcessName,
                Stacksource = data.StackSources
            };
            return log;
        }
    }
}
