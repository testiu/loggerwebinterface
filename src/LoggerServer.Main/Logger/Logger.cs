﻿using Libs.Logger;

namespace LoggerServer.Main.Logger
{
    public class Logger : Libs.Logger.Logger
    {
        private static Logger _instance = new Logger(LogLevel.FullLog, EmptyLogger);

        public static Logger Instance
        {
            get { return _instance; }
        }

        private Logger(LogLevel logLevel, ILogger innerLoggerWrapper)
            : base(logLevel, "Main", innerLoggerWrapper)
        {
        }

        [LoggerWrapperInitializationMethod]
        public static void Init(ILogger innerLogger)
        {
            _instance = new Logger(innerLogger.Level, innerLogger);
            InitializeLoggerInAssembly(_instance,typeof(RepoLogger).Assembly);
        }
    }
}
