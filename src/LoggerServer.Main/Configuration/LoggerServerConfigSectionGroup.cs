﻿using System.Configuration;

namespace LoggerServer.Main.Configuration
{
    public class LoggerServerConfigSectionGroup
    {
        public const string LoggerServerSectionGroupName = "LoggerServerConfig";


        public LoggerServerConfigurationConfigClass LoggerServerMainConfigurationSection
        {
            get
            {
                return  ConfigurationManager.GetSection(LoggerServerSectionGroupName + "/" +
                                                    "LoggerServerConfigurationSection") as
                        LoggerServerConfigurationConfigClass;
            }
        }


        public ILoggerServerConfiguration LoadLoggerServerConfigurationSection()
        {
            return LoggerServerMainConfigurationSection.ExtractConfigData();
        }
    }
}