﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Libs.Logger.Configuration;
using LoggerServer.Main.Logger.Handlers.Mongo;
using NLog.LogReceiverService;

namespace LoggerServer.Main
{
    public class LoggerServerController: IDisposable
    {
        private volatile bool _isWork = false;
        private volatile bool _isDisposed = false;

        private Libs.Logger.LoggerBase _mainLogger;
        private Libs.Logger.LoggerBase _supportLogger;

        private LoggerMongoHandler _handler;

        private Libs.Logger.Net.LoggerServer _tcpServer;
        private Libs.Logger.Net.LoggerServer _pipeServer;

        public LoggerServerController()
        {
        }


        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            _supportLogger.Fatal(e.ExceptionObject as Exception, "Unhandled exception");
        }

        public void Start()
        {
            if (_isDisposed)
                throw new ObjectDisposedException(this.GetType().Name);
            if (_isWork)
                throw new InvalidOperationException("LoggerServerController is already started");
            _isWork = true;

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            
             /* _supportLogger = Libs.Logger.LoggerFactory.CreateLoggerFromAppConfig("LoggerServer", "LoggerServerConfig", "SupportLoggerConfigurationSection");
            //_supportLogger.SetConverterFactory(new Libs.Logger.LoggingEventConverters.ConverterFactory());
            LogNs.Logger.Init(_supportLogger);

            _mainLogger = Libs.Logger.LoggerFactory.CreateLoggerFromAppConfig("LoggerServer", "LoggerServerConfig", "MainLoggerConfigurationSection");
            //_mainLogger.SetConverterFactory(new Libs.Logger.LoggingEventConverters.ConverterFactory());*/

            StartLoggerHandler();
            //StartNLogHandler();

            //_supportLogger.Debug("Logger server controller started");
        }

        private void StartNLogHandler()
        {
            /*NLogMongoHandler nlog = new NLogMongoHandler();
            string uri = @"net.tcp://0.0.0.0:7777/NlogReceiver";
            ServiceHost host = new ServiceHost(nlog);
            host.AddServiceEndpoint(typeof (ILogReceiverServer), new NetTcpBinding(), new Uri(uri));
            host.Open();*/
        }

        public void StartLoggerHandler()
        {
            _handler = new LoggerMongoHandler();

            var configLoader = new LoggerServer.Main.Configuration.LoggerServerConfigSectionGroup();
            var config = configLoader.LoadLoggerServerConfigurationSection();

            if (config.TcpServerConfig.IsEnabled)
                _tcpServer = Libs.Logger.Net.LoggerServer.CreateOnTcp(_handler, config.TcpServerConfig.Port);

            if (config.PipeServerConfig.IsEnabled)
                _pipeServer = Libs.Logger.Net.LoggerServer.CreateOnPipe(_handler, config.PipeServerConfig.PipeName);


            _tcpServer.Open();
            _pipeServer.Open();
        }


        protected void Dispose(bool isUserCall)
        {
            if (_isDisposed)
                return;

            _isDisposed = true;

            if (isUserCall)
            {
                if (_tcpServer != null)
                    _tcpServer.Dispose();

                if (_pipeServer != null)
                    _pipeServer.Dispose();

                _mainLogger.Dispose();

                _supportLogger.Debug("Logger Server Controller disposed");
                AppDomain.CurrentDomain.UnhandledException -= CurrentDomain_UnhandledException;
                _supportLogger.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
