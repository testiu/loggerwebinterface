﻿using System.Web;
using System.Web.Optimization;

namespace LoggerWebInterface
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/site-css").Include(
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
            "~/Content/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"));

            bundles.Add(new StyleBundle("~/Content/jquery-ui-css").Include(
                      "~/Content/jquery-ui/css/smoothness/jquery-ui-1.10.4.custom.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/init").Include(
            "~/Scripts/init.js"));

            bundles.Add(new ScriptBundle("~/bundles/moment").Include(
                        "~/Scripts/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/otf").Include(
             //   "~/Scripts/jquery.unobstrusive-ajax.js",
                        "~/Scripts/otf.js"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
            "~/Content/datetimepicker/jquery-ui-timepicker-addon.min.js"));

            bundles.Add(new StyleBundle("~/Content/datetimepicker-css").Include(
            "~/Content/datetimepicker/jquery-ui-timepicker-addon.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/multiselect-dropdown").Include(
                "~/Content/multiselect-dropdown/js/bootstrap-multiselect.js",
                "~/Content/multiselect-dropdown/js/prettify.js"));

            bundles.Add(new ScriptBundle("~/Content/multiselect-dropdown-css").Include(
                "~/Content/multiselect-dropdown/css/multiselect-dropdown.css",
                "~/Content/multiselect-dropdown/css/prettify.js"));

            bundles.Add(new ScriptBundle("~/bundles/labeled-checkbox-radio").Include(
                "~/Content/labeled-checkbox-radio/js/jquery-labelauty.js"));

            bundles.Add(new ScriptBundle("~/Content/labeled-checkbox-radio-css").Include(
                "~/Content/labeled-checkbox-radio/css/jquery-labelauty.css"));

            bundles.Add(new ScriptBundle("~/bundles/pie-countdown").Include(
                "~/Content/pie-countdown/js/jquery.piechartcountdown.min.js"));

            bundles.Add(new ScriptBundle("~/Content/pie-countdown-css").Include(
                "~/Content/pie-countdown/css/style.css"));

            bundles.Add(new ScriptBundle("~/bundles/json").Include(
                "~/Scripts/json2.js"));
        }
    }
}
