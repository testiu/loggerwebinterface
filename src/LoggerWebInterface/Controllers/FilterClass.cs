﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using Libs.Logger;
using LinqKit;
using LoggerWebInterface.Models;
using LoggerWebInterface.Views.Validators;
using LogRepository.Entities;
using LogRepository.Repositories;
using Microsoft.Ajax.Utilities;
using MongoDB.Bson;

namespace LoggerWebInterface.Controllers
{
    public class FilterClass
    {
        private ILogRepository db = new LogMongoDbRepository(new MongoDbRepository<Log>(), new MongoDbRepository<Host>(),
            new MongoDbRepository<Process>());

        //private static Logger _logger = Logger.Default;
        
        public FilterClass()
        {
            //TODO: remove db.LogLevel from constructor
            
            

            /*var levels = Enum.GetValues(typeof(Log.LoggerLevel))
                    .OfType<Log.LoggerLevel>()
                    .ToList();*/
            var levels = Enum.GetNames(typeof(Log.LoggerLevel)).ToList();
            LevelMultiselect = new MultiSelectItems(levels,levels);

            var hosts = db.GetAllHosts();
            HostMultiselect = new MultiSelectItems(hosts,hosts);

            var processes = db.GetAllProcessNames();
            ProcessMultiselect = new MultiSelectItems(processes,processes);

            //SelectedHosts = db.GetAllHosts().Select(h => new SelectedItemVm<string>(h)).ToList();
            MaxObjectId = "";
        }
            
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? FromTime { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? ToTime { get; set; }

        //[Regex]
        public string MessageRegex { get; set; }

        public MultiSelectItems HostMultiselect { get; set; } 
        public MultiSelectItems LevelMultiselect { get; set; }
        public MultiSelectItems ProcessMultiselect { get; set; }
        
        public IEnumerable<Log> Logs { get; set; }
        //public ObjectId MaxObjectId { get; set; }
        public string MaxObjectId { get; set; }

        public void Filter()
        {
            var query = db.AsQueryable();

            if(!string.IsNullOrEmpty(MaxObjectId))
            {
                // фильтр не поменялся
                var lastId = ObjectId.Parse(MaxObjectId);
                query = query.Where(log => log.Id > lastId);
            }

            query = PredicateTime(query);
            query = PredicateLevels(query);
            query = PredicateHosts(query);
            query = PredicateProcesses(query);
            query = PredicateMessage(query);

            query = query.OrderByDescending(p => p.Date).Take(100);

            //_logger.Info(query.ToString());
            Logs = query.ToList();
            /*foreach (var log in Logs)
            {
                log.Date = log.Date.ToUniversalTime();
            }*/
            //if(Logs.Any())
            //    MaxObjectId = Logs.Max(log => log.Id);
        }

        private IQueryable<Log> PredicateTime(IQueryable<Log> query)
        {
            if (FromTime != null)
            {
                FromTime = DateTime.SpecifyKind(FromTime.Value, DateTimeKind.Utc);
                query = query.Where(p => p.Date > FromTime);
            }
            if (ToTime != null)
            {
                ToTime = DateTime.SpecifyKind(ToTime.Value, DateTimeKind.Utc);
                query = query.Where(p => p.Date <= ToTime);
            }
            return query;
        }

        private IQueryable<Log> PredicateLevels(IQueryable<Log> query)
        {
            var levels = LevelMultiselect
                .GetConvertedSelectedItems()
                .Select(item => (Log.LoggerLevel) Enum.Parse(typeof (Log.LoggerLevel), item))
                .ToList();
            return query.Where(p => levels
                .Contains(p.LogLevel));
        }

        private IQueryable<Log> PredicateHosts(IQueryable<Log> query)
        {
            return query.Where(p => HostMultiselect
                .GetConvertedSelectedItems()
                .Contains(p.Host));
        }

        private IQueryable<Log> PredicateProcesses(IQueryable<Log> query)
        {
            return query.Where(p => ProcessMultiselect
                .GetConvertedSelectedItems()
                .Contains(p.ProcessName));
        }

        private IQueryable<Log> PredicateMessage(IQueryable<Log> query)
        {
            if (!String.IsNullOrEmpty(MessageRegex))
            {
                try
                {
                    var regex = new Regex(MessageRegex);
                }
                catch (Exception)
                {
                    return query;
                }
                query = query.Where(p => (new Regex(MessageRegex)).IsMatch(p.Message));
            }
            //query = query.Where(p => p.Message == BsonRegularExpression.Create(new Regex(MessageRegex)));
            return query;
        } 
        public Log GetLog(string id)
        {
            var objId = new ObjectId(id);
            return db.GetById(objId);
        }
    }
}