﻿$(function () {
    $(".date-time-value").datetimepicker({
        dateFormat: 'dd.mm.yy',
        timeFormat: 'HH:mm:ss'
    });

    $(document).ready(function () {
        $('.multiselect').multiselect({
            numberDisplayed: 0,
            nonSelectedText: 'Не учитывается',
            nSelectedText: 'выбрано',
            buttonWidth: false,
            buttonContainer: '<span class="dropdown" />',
            //buttonClass: 'btn-default btn-sm',
            //includeSelectAllOption: true,
            //includeSelectAllDivider: true,
            //selectAllText: 'Выбрать все',
            /*buttonText: function(options, select) {
                if (options.length == 0) {
                    return this.nonSelectedText + ' <b class="caret"></b>';
                } else {
                    var selected = '';
                    options.each(function() {
                        var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
                        selected += label + ', ';
                    });
                    var btnText = selected.substr(0, selected.length - 2) ;

                    if (textWidth(btnText) > this.buttonWidth /*|| options.length > this.numberDisplayed#1#) {
                        return options.length + ' ' + this.nSelectedText + ' <b class="caret"></b>';
                    } else {
                        return btnText + ' <b class="caret"></b>';
                    }
                }
            }*/
        });
        
    });

    $(".tooltip-info").tooltip({
        placement: "right"
    });

    /*var textWidth = function (data) {
        // TODO: расчет ширины текста в пикселях на реальной странице
        //var htmlCalc = $("<span>").attr('id', 'htmlCalc').html('<span>' + data + '</span>').width();
        //var width = $('#htmlCalc').innerWidth();
        var pxPerLetter = 7;
        var width = data.length * pxPerLetter;
        return width;
    };*/
});