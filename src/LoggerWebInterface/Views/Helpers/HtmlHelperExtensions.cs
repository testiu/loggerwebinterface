﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LogRepository.Entities;

namespace LoggerWebInterface.Views.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static string ExceptionTree(this HtmlHelper html, LogException exception)
        {
            string htmlOutput = string.Empty;

            if (exception != null)
            {
                htmlOutput += "<dd><pre>";
                if (!String.IsNullOrEmpty(exception.Message))
                {
                    htmlOutput += "<em>Сообщение</em> <p>";
                    htmlOutput += exception.Message;
                    htmlOutput += "</p>";
                }
                if (!String.IsNullOrEmpty(exception.Source))
                {
                    htmlOutput += "<em>Источник</em> <p>";
                    htmlOutput += exception.Source;
                    htmlOutput += "</p>";
                }
                if (!String.IsNullOrEmpty(exception.StackTrace))
                {
                    htmlOutput += "<em>Stack Trace</em> <p>";
                    htmlOutput += exception.StackTrace;
                    htmlOutput += "</p>";
                }
                if (!String.IsNullOrEmpty(exception.Type))
                {
                    htmlOutput += "<em>Тип</em> <p>";
                    htmlOutput += exception.Type;
                    htmlOutput += "</p>";
                }

                if (exception.InnerException != null)
                {
                    htmlOutput += "<em>Внутреннее исключение</em> <p>";
                    htmlOutput += html.ExceptionTree(exception.InnerException);
                    htmlOutput += "</p>";
                }
            }

            return htmlOutput;
        }

    }
}