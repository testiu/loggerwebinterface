﻿using System.Collections.Generic;
using LogRepository.Entities;

namespace LoggerWebInterface.Views.Helpers
{
    public class LogHelpers
    {
        private static readonly Dictionary<Log.LoggerLevel, string> colorAssosiations = new Dictionary
            <Log.LoggerLevel, string>
        {
            {Log.LoggerLevel.Trace, ""},
            {Log.LoggerLevel.Debug, ""},
            {Log.LoggerLevel.Info, ""},
            {Log.LoggerLevel.Warn, ""},
            {Log.LoggerLevel.Error, ""},
            {Log.LoggerLevel.Fatal, ""}
        };

        private static readonly Dictionary<Log.LoggerLevel, string> classAssosiations = new Dictionary
            <Log.LoggerLevel, string>
        {
            {Log.LoggerLevel.Trace, "label label-default label-level default-level"},
            {Log.LoggerLevel.Debug, "label label-default label-level default-level"},
            {Log.LoggerLevel.Info, "label label-default label-level default-level"},
            {Log.LoggerLevel.Warn, "label label-warning label-level warn-level"},
            {Log.LoggerLevel.Error, "label label-danger label-level error-level"},
            {Log.LoggerLevel.Fatal, "label label-default label-level fatal-level"}
        };

        public static string ColorLevel(Log.LoggerLevel level)
        {
            return colorAssosiations[level];
        }

        public static string ClassLevel(Log.LoggerLevel level)
        {
            return classAssosiations[level];
        }
    }
}