﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LoggerWebInterface.Views.Helpers
{
    public static class ExtansionClass
    {
        public static string ToSafeString<T>(this T obj, string nullString="null")
        {
            return obj != null ? obj.ToString() : "null";
        }

        public static object ToObjectFromString<T>(this object str)
        {
            return str != "null" ? str : null;
        }
    }
}