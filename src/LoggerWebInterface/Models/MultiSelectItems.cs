﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LoggerWebInterface.Views.Helpers;

namespace LoggerWebInterface.Models
{
    public class MultiSelectItems
    {
        private List<string> _allItems;
        private string _nullString;
        public MultiSelectItems(IEnumerable<string> allItems, IEnumerable<string> defaultItems, string nullString="null")
        {
            _nullString = nullString;
            _allItems = allItems.ToList();
            SelectedItems = defaultItems != null ? defaultItems.Select(ToSafeString).ToList() : new List<string>();
        }

        public MultiSelectItems() { }

        public List<string> AllItems
        {
            get { return _allItems.Select(ToSafeString).ToList(); }
        }
        public List<string> SelectedItems { get; set; }

        public List<string> GetConvertedSelectedItems()
        {
            return SelectedItems.Select(NullableString).ToList();
        }

        public string ToSafeString(string str)
        {
            return str ?? _nullString;
        }

        public string NullableString(string str)
        {
            return str != _nullString ? str : null;
        }
    }
}